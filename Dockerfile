# vim: ft=dockerfile
FROM debian:bullseye

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get -q install --no-install-recommends -y gnupg dirmngr ca-certificates dpkg && \
    APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 06E85760C0A52C50 && \
    echo 'deb http://www.ui.com/downloads/unifi/debian stable ubiquiti' | tee /etc/apt/sources.list.d/ubnt-unifi.list && \
    apt-get update && \
    rm -fr /var/lib/apt/lists/* /usr/share/doc/* && \
    true

ADD repackage.sh /

